# defining the function
def collatz():
    global number
    while number!=1:
        if number%2==0:
            print (number//2)
            number=number//2
        elif number%2==1:
            print (3*number+1)
            number=number*3+1
    # else:
    #    print ('that is not an integer mate!')

# asking for input
print('Please type an integer: ')
number=int(input())
print('your integer is '+str(number))

#crunching the numbers
collatz()           # all I had to do was call the function I made??

# printing the output

# Done inside the function.
# Maybe I should do a different version that streamlines the collatz() function?
