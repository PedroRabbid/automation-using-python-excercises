#Conway's Game of Life
import random, time, copy
WIDTH=60
HEIGHT=20
#Create a list of list for the cells
nextCells=[]
for x in range(WIDTH):
    column=[] #create a new column.
    for y in range(HEIGHT):
        if random.randint(0, 1)==0:
            column.append('#') #add a living cell.
        else:
            column.append(' ') #add a dead cell.
    nextCells.append(column) #nextCells is a list
while True: #Main program loop
    print('\n\n\n\n\n') #separate each step with newlines.
    currentCells=copy.deepcopy(nextCells)
    # Print currentCells on the screen:
    for y in range(HEIGHT):
        for x in range(WIDTH):
            print(currentCells[x][y],end='') #Print the # of space.
        print() #Print a newline at the end of the row.
    #Calculate the next step's cells based on current step's cels
    for x in range(WIDTH):
        for y in range(HEIGHT):
            # Get neigtbouring coordinates
            # '%WIDTH' ensures leftCoord is always between 0 and WIDTH -1
            leftCoord=(x-1)%WIDTH
            rightCoord=(x+1)%WIDTH
            aboveCoord=(y-1)%HEIGHT
            belowCoord=(y+1)%HEIGHT
            # Count number of living neighbours
            numNeighbours=0
            if currentCells[leftCoord][aboveCoord]=='#':
                numNeighbours+=1 #Top-left neighbour is alive!
            if currentCells[x][aboveCoord]=='#':
                numNeighbours+=1 #Top Neighbour is alive!
            if currentCells[rightCoord][aboveCoord]=='#':
                numNeighbours+=1 # Top-right neighbor is alive.
            if currentCells[leftCoord][y]=='#':
                numNeighbours+=1 # Left neighbor is alive.
            if currentCells[rightCoord][y]=='#':
                numNeighbours+=1 # Right neighbor is alive.
            if currentCells[leftCoord][belowCoord]=='#':
                numNeighbours+=1 # Bottom-left neighbor is alive.
            if currentCells[x][belowCoord]=='#':
                numNeighbours+=1 # Bottom neighbor is alive.
            if currentCells[rightCoord][belowCoord]=='#':
                numNeighbours+=1 # Bottom-right neighbor is alive.

            # Set cell based on Conway's Game of Life rules:
            if currentCells[x][y]=='#' and (numNeighbours==2 or numNeighbours==3):
                # Living cells with 2 or 3 neighbors stay alive:
                nextCells[x][y]='#'
            elif currentCells[x][y]==' ' and numNeighbours==3:
                # Dead cells with 3 neighbors become alive:
                nextCells[x][y]='#'
            else:
                # Everything else dies or stays dead:
                nextCells[x][y]=' '
time.sleep(1) # Add a 1-second pause to reduce flickering.
            
